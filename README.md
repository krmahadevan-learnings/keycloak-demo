## OAuth2 and OpenID Connect

### Reference Youtube videos

* [OAuth 2.0 and OpenID Connect (in plain English)](https://www.youtube.com/watch?v=996OiexHze0)
* [An Illustrated Guide to OAuth and OpenID Connect](https://www.youtube.com/watch?v=t18YB3xDfXI)
* [Spring boot 3 Keycloak integration for beginners | The complete Guide](https://www.youtube.com/watch?v=vmEWywGzWbA)


### Tools and Reference materials

* [OpenID Connect Debugger](https://oidcdebugger.com/)
* [OAuth 2.0 `<debugger/>`](https://oauthdebugger.com/)
* [OAuth 2.0 Playground](https://www.oauth.com/playground/)
* [OAuth 2.0 Simplified](https://www.oauth.com/)
* [OAuth 2.0 Simplified for PDF/Kindle](https://oauth2simplified.com/)
* [Spring Security](https://docs.spring.io/spring-security/reference/index.html)
* [Spring Security Samples](https://github.com/spring-projects/spring-security-samples/tree/main)
* [OAuth 2.0 Tools and Libraries](https://www.oauth.com/oauth2-servers/tools-and-libraries/)


### Steps

* Boot up the Keycloak container by running `docker-compose -f keycloak-docker-compose.yaml up -d` from the directory `src/main/resources`
* Login to the keycloak instance using `admin`/`admin`
* Import the realm and the user's json 
* Run the main method.
* Now run the below curl command to generate a token.

```shell
curl --request POST \
  --url http://localhost:8080/realms/necropolis/protocol/openid-connect/token \
  --header 'Content-Type: application/x-www-form-urlencoded' \
  --data grant_type=password \
  --data client_id=cadaver-rest-api \
  --data username=cadaver \
  --data password=password
```

* Now you can invoke the below URL to see the roles in action. Don't forget to replace the token with the token you got from the previous call.

```shell
curl --request GET \
  --url http://localhost:9090/api/v1/demo/greetings \
  --header 'Authorization: Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIxazJoRlRmRzc1TWctcE5mdmVKSlkzRENUV2dHbmk5emdQcHl1SFVudzhnIn0.eyJleHAiOjE2OTUyOTU1MzIsImlhdCI6MTY5NTI5NTIzMiwianRpIjoiODM4NWUzZmYtODZlNi00YTM4LTgzNzQtMTIzNmQ2ZjExMTQ2IiwiaXNzIjoiaHR0cDovL2xvY2FsaG9zdDo4MDgwL3JlYWxtcy9uZWNyb3BvbGlzIiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjBlN2QxN2JiLTFlYWEtNGUwMi1hMDU0LTBkNWM5YWI4ZjE1MCIsInR5cCI6IkJlYXJlciIsImF6cCI6ImNhZGF2ZXItcmVzdC1hcGkiLCJzZXNzaW9uX3N0YXRlIjoiYWFhZWNkMjYtNDA0NC00ZmVhLWIzODktM2IwZDVhMDkwZTgyIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsInVtYV9hdXRob3JpemF0aW9uIiwiZGVmYXVsdC1yb2xlcy1uZWNyb3BvbGlzIiwiYWRtaW5fdXNlciJdfSwicmVzb3VyY2VfYWNjZXNzIjp7ImNhZGF2ZXItcmVzdC1hcGkiOnsicm9sZXMiOlsiY2FkYXZlcl9hZG1pbiJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwic2lkIjoiYWFhZWNkMjYtNDA0NC00ZmVhLWIzODktM2IwZDVhMDkwZTgyIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJjYWRhdmVyIiwiZ2l2ZW5fbmFtZSI6IiIsImZhbWlseV9uYW1lIjoiIn0.FrKtiF2CpmUPFbHoZXwTZnmZjaZD8YiiVExAu--lpUOStoApF3YrPUivIa9VWpyB4PZtOulSRCTQ0GyG8h286zG3gk7ZTLq7XnNHMCM85qxnYjTfwhPO5mgp8nGfCWq50p-rFdl9UMvY61WW4itkbrFLAhEKDiaQS5v7gq2rGcuWudGDJtoAdZMY-N70OxdYwsmJ7dMzxRxVywjMZfKzSkVj-NTbARR4EadDHjhZU93fuos8De2urpWogjuCMEMxSFG3kLP-Ybi1EZvmJp4uXImqU-FzCxMKHAE-aRYg6E0Vd90GW-KOZynfNNoXI0j-BamXzxyJUkFXA4ODA23Vew' \
  --header 'Content-Type: application/x-www-form-urlencoded'
```