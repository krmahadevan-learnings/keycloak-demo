package com.rationaleemotions.keycloakdemo;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/demo")
public class DemoController {

    @GetMapping
    @PreAuthorize("hasAuthority('cadaver_user')")
    public String hello() {
        return "Hello from Keycloak demo";
    }

    @GetMapping("/greetings")
    @PreAuthorize("hasAuthority('cadaver_admin')")
    public String greeting() {
        return "Greetings from Keycloak demo (Admin)";
    }
}
