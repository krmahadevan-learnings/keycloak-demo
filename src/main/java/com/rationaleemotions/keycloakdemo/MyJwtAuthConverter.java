package com.rationaleemotions.keycloakdemo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtClaimNames;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MyJwtAuthConverter implements Converter<Jwt, AbstractAuthenticationToken> {

    private final JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter =
            new JwtGrantedAuthoritiesConverter();

    @Value("${jwt.auth.converter.principle-attribute}")
    private String principleAttribute;
    @Value("${jwt.auth.converter.resource-id}")
    private String resourceId;

    @Override
    public AbstractAuthenticationToken convert(@NonNull Jwt jwt) {
        Collection<GrantedAuthority> authorities = Stream.concat(
                jwtGrantedAuthoritiesConverter.convert(jwt).stream(), //This will give all the well known scopes
                extractResourceRoles(jwt).stream() // This gives our custom scopes
        ).collect(Collectors.toSet());

        return new JwtAuthenticationToken(
                jwt,
                authorities,
                getPrincipleClaimName(jwt)
        );
    }

    private String getPrincipleClaimName(Jwt jwt) {
        String claimName = Optional.ofNullable(principleAttribute).orElse(JwtClaimNames.SUB);
        return jwt.getClaim(claimName);
    }

// This is the JSON payload that we are working with
// "resource_access" : {
//        "account" : {
//            "roles" : ["manage-account","manage-account-links","view-profile"]
//        },
//        "cadaver-rest-api" : {
//            "roles" : ["cadaver_admin"]
//        }
//    }
    @SuppressWarnings("unchecked")
    private Collection<? extends GrantedAuthority> extractResourceRoles(Jwt jwt) {
        return Optional.ofNullable(jwt.getClaim("resource_access"))
                .map(found -> (Map<String, Object>)found)
                .map(found -> found.get(resourceId))
                .map(found -> (Map<String, Object>)found)
                .map(found -> (Collection<String>)found.get("roles"))
                .orElse(Set.of())
                .stream()
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
    }
}